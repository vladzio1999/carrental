package com.carrental.car;

import javax.persistence.*;

import com.carrental.api.car.CarGetRequest;
import com.carrental.api.car.CarStatus;
import org.hibernate.annotations.GenericGenerator;

import java.util.Objects;

@Entity
@Table(name = "cars")
public class CarEntity {

    @Column(name = "id", nullable = false, unique = true)
    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    private Long id;
    private String brand;
    private String model;
    private CarStatus carStatus;

    public CarEntity() {}

    public CarEntity(String brand, String model, CarStatus carStatus) {
        this.brand = brand;
        this.model = model;
        this.carStatus = carStatus;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public CarStatus getCarStatus() {
        return carStatus;
    }

    public void setCarStatus(CarStatus carStatus) {
        this.carStatus = carStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CarEntity)) return false;
        CarEntity carEntity = (CarEntity) o;
        return id.equals(carEntity.id) &&
                Objects.equals(brand, carEntity.brand) &&
                Objects.equals(model, carEntity.model) &&
                carStatus == carEntity.carStatus;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public CarGetRequest toCarGetRequest() {
        return new CarGetRequest(this);
    }

}
