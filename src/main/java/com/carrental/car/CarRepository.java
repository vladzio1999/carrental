package com.carrental.car;

import com.carrental.api.car.CarStatus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.NoSuchElementException;

public interface CarRepository extends JpaRepository<CarEntity, Long> {
    List<CarEntity> findByCarStatus(CarStatus carStatus);
}

