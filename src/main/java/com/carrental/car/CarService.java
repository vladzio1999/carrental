package com.carrental.car;

import com.carrental.api.car.CarGetRequest;
import com.carrental.api.car.CarPostRequest;
import com.carrental.api.car.CarSetStatusRequest;
import com.carrental.api.car.CarStatus;

import java.util.List;

public interface CarService {
    CarPostRequest createCar(CarPostRequest carPostRequest);
    CarGetRequest findCar(Long id);
    void deleteCar(Long id);
    List<CarGetRequest> findAllCars();
    void setCarStatus(Long id, CarSetStatusRequest carSetStatusRequest);
    List<CarGetRequest> findAvailableCars();
    boolean exist(Long id);
}
