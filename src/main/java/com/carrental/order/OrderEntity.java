package com.carrental.order;

import com.carrental.api.order.OrderClientGetRequest;
import com.carrental.api.order.OrderGetRequest;
import com.carrental.car.CarEntity;
import com.carrental.client.ClientEntity;
import com.fasterxml.jackson.annotation.JsonBackReference;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "orders")
public class OrderEntity {

    @Column(name = "id", nullable = false, unique = true)
    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "client_id")
    private ClientEntity clientEntity;

    @OneToOne
    @JoinColumn(name = "car_id")
    private CarEntity carEntity;
    private Integer term;

    public OrderEntity() {}

    public OrderEntity(CarEntity carEntity, ClientEntity clientEntity, Integer term) {
        this.carEntity = carEntity;
        this.clientEntity = clientEntity;
        this.term = term;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ClientEntity getClientEntity() {
        return clientEntity;
    }

    public void setClientEntity(ClientEntity clientEntity) {
        this.clientEntity = clientEntity;
    }

    public CarEntity getCarEntity() {
        return carEntity;
    }

    public void setCarEntity(CarEntity carEntity) {
        this.carEntity = carEntity;
    }

    public Integer getTerm() {
        return term;
    }

    public void setTerm(Integer term) {
        this.term = term;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OrderEntity)) return false;
        OrderEntity orderEntity = (OrderEntity) o;
        return id.equals(orderEntity.id) &&
                Objects.equals(clientEntity, orderEntity.clientEntity) &&
                Objects.equals(carEntity, orderEntity.carEntity) &&
                Objects.equals(term, orderEntity.term);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }


    public OrderGetRequest toOrderGetRequest() {
        return new OrderGetRequest(this);
    }

    public OrderClientGetRequest toOrderClientGetRequest() {
        return new OrderClientGetRequest(this);
    }
}
