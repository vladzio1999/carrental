package com.carrental.order;

import com.carrental.api.order.OrderGetRequest;
import com.carrental.api.order.OrderPostRequest;

import java.util.List;

public interface OrderService {
    OrderPostRequest createOrder(OrderPostRequest order);
    OrderGetRequest findOrder(Long id);
    OrderPostRequest editOrder(Long id, OrderPostRequest orderPostRequest);
    void deleteOrder(Long id);
    List<OrderGetRequest> findAllOrders();
    boolean exist(Long id);
}
