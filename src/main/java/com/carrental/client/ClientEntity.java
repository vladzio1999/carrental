package com.carrental.client;

import com.carrental.api.client.ClientGetRequest;
import com.carrental.order.OrderEntity;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "clients")
public class ClientEntity {

    @Column(name = "id", nullable = false, unique = true)
    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    private Long id;
    private String firstName;
    private String lastName;
    private String dateOfBirth;

    @OneToMany(mappedBy = "clientEntity", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @Column(name = "orders")
    private List<OrderEntity> orderEntities = new ArrayList<>();


    public ClientEntity() {}

    public ClientEntity(String firstName, String lastName, String dateOfBirth) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
    }

    public ClientEntity(String firstName, String lastName, String dateOfBirth, List<OrderEntity> orderEntities) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.orderEntities = orderEntities;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public List<OrderEntity> getOrderEntities() {
        return orderEntities;
    }

    public void setOrderEntities(List<OrderEntity> orderEntities) {
        this.orderEntities = orderEntities;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ClientEntity)) return false;
        ClientEntity clientEntity = (ClientEntity) o;
        return Objects.equals(id, clientEntity.id) &&
                Objects.equals(firstName, clientEntity.firstName) &&
                Objects.equals(lastName, clientEntity.lastName) &&
                Objects.equals(dateOfBirth, clientEntity.dateOfBirth) &&
                Objects.equals(orderEntities, clientEntity.orderEntities);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public ClientGetRequest toClientGetRequest() {
        return new ClientGetRequest(this);
    }
}
