package com.carrental.client;

import com.carrental.api.client.ClientGetRequest;
import com.carrental.api.client.ClientPostRequest;
import com.carrental.api.order.OrderClientGetRequest;

import java.util.List;

public interface ClientService {
    ClientPostRequest createClient(ClientPostRequest clientPostRequest);
    ClientGetRequest findClient(Long id);
    ClientPostRequest editClient(Long id, ClientPostRequest clientPostRequest);
    void deleteClient(Long id);
    List<ClientGetRequest> findAllClients();
    List<OrderClientGetRequest> findAllOrders(Long id);
    boolean exist(Long id);
}
