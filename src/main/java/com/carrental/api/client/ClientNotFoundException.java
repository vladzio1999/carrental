package com.carrental.api.client;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ClientNotFoundException extends NullPointerException {

    public ClientNotFoundException(Long id) {
        super("Client " + id + " not found");
    }
}
