package com.carrental.api.client;

import com.carrental.client.ClientEntity;
import com.carrental.client.ClientRepository;
import com.carrental.client.ClientService;
import com.carrental.api.order.OrderClientGetRequest;
import com.carrental.order.OrderEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class ClientServiceImpl implements ClientService {

    @Autowired
    private ClientRepository clientRepository;

    @Override
    public ClientPostRequest createClient(ClientPostRequest clientPostRequest) {
        ClientEntity clientEntity = new ClientEntity();
        clientEntity.setFirstName(clientPostRequest.getFirstName());
        clientEntity.setLastName(clientPostRequest.getLastName());
        clientEntity.setDateOfBirth(clientPostRequest.getDateOfBirth());
        clientRepository.save(clientEntity);
        return clientPostRequest;
    }

    @Override
    public ClientGetRequest findClient(Long id) {
        try {
            return clientRepository.findById(id).get().toClientGetRequest();
        } catch (NoSuchElementException ex) {
            return null;
        }
    }

    @Override
    public ClientPostRequest editClient(Long id, ClientPostRequest clientPostRequest) {

        ClientEntity clientEntity = new ClientEntity();
        clientEntity.setId(id);
        clientEntity.setFirstName(clientPostRequest.getFirstName());
        clientEntity.setLastName(clientPostRequest.getLastName());
        clientEntity.setDateOfBirth(clientPostRequest.getDateOfBirth());
        clientEntity.setOrderEntities(clientRepository.findById(id).get().getOrderEntities());
        clientRepository.save(clientEntity);
        return clientPostRequest;
    }

    @Override
    public void deleteClient(Long id) {
        try {
            clientRepository.deleteById(id);
        } catch (EmptyResultDataAccessException ex ) {
            throw new ClientNotFoundException(id);
        }

    }

    @Override
    public List<ClientGetRequest> findAllClients() {
        return clientRepository.findAll().stream()
                .map(ClientGetRequest::new).collect(Collectors.toList());
    }

    @Override
    public List<OrderClientGetRequest> findAllOrders(Long id) {
        return clientRepository.findById(id).map(client -> client.getOrderEntities().stream()
                .map(OrderClientGetRequest::new)).orElseThrow(() -> new ClientNotFoundException(id))
                .collect(Collectors.toList());
    }

    public boolean exist(Long id) {
        try {
            clientRepository.findById(id).get();
        } catch (NoSuchElementException ex) {
            return false;
        }
        return true;
    }
}
