package com.carrental.api.client;

import com.carrental.client.ClientEntity;

public class ClientGetRequest {

    private Long id;
    private String firstName;
    private String lastName;
    private String dateOfBirth;

    public ClientGetRequest() {}

    public ClientGetRequest(ClientEntity clientEntity) {
        this.id = clientEntity.getId();
        this.firstName = clientEntity.getFirstName();
        this.lastName = clientEntity.getLastName();
        this.dateOfBirth = clientEntity.getDateOfBirth();
    }

    public ClientGetRequest(Long id, String firstName, String lastName, String dateOfBirth) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

}
