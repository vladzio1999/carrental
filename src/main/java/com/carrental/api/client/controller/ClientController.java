package com.carrental.api.client.controller;


import com.carrental.api.client.ClientNotFoundException;
import com.carrental.api.client.ClientPostRequest;
import com.carrental.api.client.ClientGetRequest;
import com.carrental.api.order.OrderClientGetRequest;
import com.carrental.client.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = ClientController.BASE_URL, produces = MediaType.APPLICATION_JSON_VALUE)
public class ClientController {

    public static final String BASE_URL = "/clients";

    @Autowired
    private ClientService clientService;

    @GetMapping
    ResponseEntity<List<ClientGetRequest>> getAllClients() {
        return new ResponseEntity<>(clientService.findAllClients(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    ResponseEntity<ClientGetRequest> getClient(@PathVariable Long id) {
        ClientGetRequest client = clientService.findClient(id);
        if( client == null) {
            throw new ClientNotFoundException(id);
        }
        return new ResponseEntity<>(client, HttpStatus.OK);
    }

    @GetMapping("/{id}/orders")
    ResponseEntity<List<OrderClientGetRequest>> getAllOrders(@PathVariable Long id) {
        return new ResponseEntity<>(clientService.findAllOrders(id), HttpStatus.OK);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    ResponseEntity<ClientPostRequest> addClient(@RequestBody ClientPostRequest clientPostRequest) {
        clientService.createClient(clientPostRequest);
        return new ResponseEntity<>(clientPostRequest, HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    ResponseEntity<HttpStatus> deleteClient(@PathVariable Long id) {
        clientService.deleteClient(id);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @PutMapping("/{id}")
    ResponseEntity<ClientPostRequest> editClient(@PathVariable Long id, @RequestBody ClientPostRequest clientPostRequest) {
        clientService.editClient(id, clientPostRequest);
        return new ResponseEntity<>(clientPostRequest, HttpStatus.OK);
    }
}
