package com.carrental.api.order;

import com.carrental.api.car.CarGetRequest;
import com.carrental.order.OrderEntity;

public class OrderClientGetRequest {

    private Long id;
    private CarGetRequest car;
    private Integer term;

    public OrderClientGetRequest() {}

    public OrderClientGetRequest(OrderEntity orderEntity) {
        this.id = orderEntity.getId();
        this.car = orderEntity.getCarEntity().toCarGetRequest();
        this.term = orderEntity.getTerm();
    }

    public OrderClientGetRequest(Long id, CarGetRequest car, Integer term) {
        this.id = id;
        this.car = car;
        this.term = term;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CarGetRequest getCar() {
        return car;
    }

    public void setCar(CarGetRequest car) {
        this.car = car;
    }

    public Integer getTerm() {
        return term;
    }

    public void setTerm(Integer term) {
        this.term = term;
    }
}
