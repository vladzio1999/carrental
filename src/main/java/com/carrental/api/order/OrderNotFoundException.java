package com.carrental.api.order;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class OrderNotFoundException extends NullPointerException {

    public OrderNotFoundException(Long id) {
        super("Order " + id + " not found");
    }
}
