package com.carrental.api.order;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class ZeroOrNegativeTermException extends RuntimeException {

    public ZeroOrNegativeTermException() {
        super("Term cannot be negative or zero");

    }
}
