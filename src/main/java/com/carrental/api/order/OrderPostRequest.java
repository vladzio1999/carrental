package com.carrental.api.order;

public class OrderPostRequest {

    private Long id;
    private Long clientId;
    private Long carId;
    private Integer term;

    public OrderPostRequest() {}

    public OrderPostRequest(Long clientId, Long carId, Integer term) {
        this.clientId = clientId;
        this.carId = carId;
        this.term = term;
    }

    public OrderPostRequest(Long id, Long clientId, Long carId, Integer term) {
        this.id = id;
        this.clientId = clientId;
        this.carId = carId;
        this.term = term;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public Long getCarId() {
        return carId;
    }

    public void setCarId(Long carId) {
        this.carId = carId;
    }

    public Integer getTerm() {
        return term;
    }

    public void setTerm(Integer term) {
        this.term = term;
    }
}
