package com.carrental.api.order.controller;

import com.carrental.api.order.OrderGetRequest;
import com.carrental.api.order.OrderNotFoundException;
import com.carrental.api.order.OrderPostRequest;
import com.carrental.order.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(OrderController.BASE_URL)
public class OrderController {

    public static final String BASE_URL = "/orders";

    @Autowired
    private OrderService orderService;

    @GetMapping
    ResponseEntity<List<OrderGetRequest>> getAllOrders() {
        return new ResponseEntity<>(orderService.findAllOrders(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    ResponseEntity<OrderGetRequest> getOrder(@PathVariable Long id) {
        OrderGetRequest order = orderService.findOrder(id);
        if (order == null) {
            throw new OrderNotFoundException(id);
        }
        return new ResponseEntity<>(orderService.findOrder(id), HttpStatus.OK);
    }

    @PostMapping
    ResponseEntity<OrderPostRequest> addOrder(@RequestBody OrderPostRequest orderPostRequest) {
        orderService.createOrder(orderPostRequest);
        return new ResponseEntity<>(orderPostRequest, HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    ResponseEntity<HttpStatus> deleteOrder(@PathVariable Long id) {
        orderService.deleteOrder(id);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @PutMapping("/{id}")
    ResponseEntity<OrderPostRequest> editOrder(@PathVariable Long id, @RequestBody OrderPostRequest orderPostRequest) {
        orderService.editOrder(id, orderPostRequest);
        return new ResponseEntity<>(orderPostRequest, HttpStatus.OK);
    }
}
