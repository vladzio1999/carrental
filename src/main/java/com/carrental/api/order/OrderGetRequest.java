package com.carrental.api.order;

import com.carrental.api.car.CarGetRequest;
import com.carrental.api.client.ClientGetRequest;
import com.carrental.order.OrderEntity;


public class OrderGetRequest {

    private Long id;
    private ClientGetRequest client;
    private CarGetRequest car;
    private Integer term;

    public OrderGetRequest() {}

    public OrderGetRequest(OrderEntity orderEntity) {
        this.id = orderEntity.getId();
        this.client = new ClientGetRequest(orderEntity.getClientEntity());
        this.car = new CarGetRequest(orderEntity.getCarEntity());
        this.term = orderEntity.getTerm();
    }

    public OrderGetRequest(Long id, ClientGetRequest clientGetRequest, CarGetRequest carGetRequest, Integer term) {
        this.id = id;
        this.client = clientGetRequest;
        this.car = carGetRequest;
        this.term = term;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ClientGetRequest getClientGetRequest() {
        return client;
    }

    public void setClientGetRequest(ClientGetRequest clientGetRequest) {
        this.client = clientGetRequest;
    }

    public CarGetRequest getCarGetRequest() {
        return car;
    }

    public void setCarGetRequest(CarGetRequest carGetRequest) {
        this.car = carGetRequest;
    }

    public Integer getTerm() {
        return term;
    }

    public void setTerm(Integer term) {
        this.term = term;
    }
}
