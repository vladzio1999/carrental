package com.carrental.api.order;

import com.carrental.api.car.CarIsUnavailableException;
import com.carrental.api.car.CarNotFoundException;
import com.carrental.api.car.CarStatus;
import com.carrental.car.CarRepository;
import com.carrental.api.client.ClientNotFoundException;
import com.carrental.client.ClientRepository;
import com.carrental.order.OrderEntity;
import com.carrental.order.OrderRepository;
import com.carrental.order.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private CarRepository carRepository;

    @Autowired
    private ClientRepository clientRepository;

    @Override
    public OrderPostRequest createOrder(OrderPostRequest orderPostRequest) {
        if (orderPostRequest.getTerm() <= 0) {
            throw new ZeroOrNegativeTermException();
        } else {
            OrderEntity orderEntity = new OrderEntity();
            orderEntity.setClientEntity(clientRepository.findById(orderPostRequest.getClientId())
                    .orElseThrow(() -> new ClientNotFoundException(orderPostRequest.getClientId())));

            if (carRepository.findById(orderPostRequest.getCarId()).get()
                    .getCarStatus() == CarStatus.UNAVAILABLE) {
                throw new CarIsUnavailableException(orderPostRequest.getCarId());
            }

            orderEntity.setCarEntity(carRepository.findById(orderPostRequest.getCarId())
                    .orElseThrow(() -> new CarNotFoundException(orderPostRequest.getCarId())));
            carRepository.findById(orderPostRequest.getCarId()).get().setCarStatus(CarStatus.UNAVAILABLE);
            orderEntity.setTerm(orderPostRequest.getTerm());
            orderRepository.save(orderEntity);
            return orderPostRequest;
        }
    }

    @Override
    public OrderGetRequest findOrder(Long id) {
        try {
            return orderRepository.findById(id).get().toOrderGetRequest();
        } catch (NoSuchElementException ex) {
            return null;
        }
    }

    @Override
    public List<OrderGetRequest> findAllOrders() {
        return orderRepository.findAll().stream()
                .map(OrderGetRequest::new).collect(Collectors.toList());
    }

    @Override
    public OrderPostRequest editOrder(Long id, OrderPostRequest orderPostRequest) {
        if (orderPostRequest.getTerm() <= 0) {
            throw new ZeroOrNegativeTermException();
        } else {
            OrderEntity orderEntity = new OrderEntity();
            orderEntity.setId(id);
            orderEntity.setClientEntity(clientRepository.findById(orderPostRequest.getClientId())
                    .orElseThrow(() -> new ClientNotFoundException(orderPostRequest.getClientId())));
            carRepository.findById(orderRepository.findById(id).get().getCarEntity().getId()).get()
                    .setCarStatus(CarStatus.AVAILABLE);

            if (carRepository.findById(orderPostRequest.getCarId()).get()
                    .getCarStatus() == CarStatus.UNAVAILABLE) {
                throw new CarIsUnavailableException(orderPostRequest.getCarId());
            }

            orderEntity.setCarEntity(carRepository.findById(orderPostRequest.getCarId())
                    .orElseThrow(() -> new CarNotFoundException(orderPostRequest.getCarId())));
            carRepository.findById(orderPostRequest.getCarId()).get().setCarStatus(CarStatus.UNAVAILABLE);
            orderEntity.setTerm(orderPostRequest.getTerm());
            orderRepository.save(orderEntity);
            return orderPostRequest;
        }
    }

    @Override
    public void deleteOrder(Long id) {
        carRepository.findById(orderRepository.findById(id).get().getCarEntity().getId()).get()
                .setCarStatus(CarStatus.AVAILABLE);
        orderRepository.deleteById(id);
    }

    @Override
    public boolean exist(Long id) {
        try {
            orderRepository.findById(id).get();
        } catch (NoSuchElementException ex) {
            return false;
        }
        return true;
    }
}
