package com.carrental.api.car;

import com.carrental.car.CarEntity;
import com.fasterxml.jackson.annotation.JsonTypeName;

import java.util.Objects;


public class CarGetRequest {

    private Long id;
    private String brand;
    private String model;
    private CarStatus carStatus;

    public CarGetRequest() {}

    public CarGetRequest(CarEntity carEntity) {
        id = carEntity.getId();
        brand = carEntity.getBrand();
        model = carEntity.getModel();
        carStatus = carEntity.getCarStatus();
    }

    public CarGetRequest(Long id, String brand, String model, CarStatus carStatus) {
        this.id = id;
        this.brand = brand;
        this.model = model;
        this.carStatus = carStatus;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public CarStatus getCarStatus() {
        return carStatus;
    }

    public void setCarStatus(CarStatus carStatus) {
        this.carStatus = carStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CarGetRequest)) return false;
        CarGetRequest that = (CarGetRequest) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(brand, that.brand) &&
                Objects.equals(model, that.model) &&
                carStatus == that.carStatus;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "{" +
                "id=" + id +
                ", brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", carStatus=" + carStatus +
                '}';
    }
}
