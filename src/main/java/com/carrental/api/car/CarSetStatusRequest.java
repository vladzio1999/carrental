package com.carrental.api.car;

public class CarSetStatusRequest {

    private CarStatus carStatus;

    public CarSetStatusRequest() {}

    public CarSetStatusRequest(CarStatus carStatus) {
        this.carStatus = carStatus;
    }

    public CarStatus getCarStatus() {
        return carStatus;
    }

    public void setCarStatus(CarStatus carStatus) {
        this.carStatus = carStatus;
    }
}
