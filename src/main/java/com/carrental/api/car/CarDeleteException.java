package com.carrental.api.car;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.FORBIDDEN)
public class CarDeleteException extends RuntimeException {

    public CarDeleteException(Long id) {
        super("Car " + id + " is in order and cannot be deleted");
    }
}
