package com.carrental.api.car;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class CarIsUnavailableException extends RuntimeException {

    public CarIsUnavailableException(Long id) {
        super("Car " + id + " is unavailable");
    }
}
