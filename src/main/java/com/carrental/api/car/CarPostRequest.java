package com.carrental.api.car;

import com.carrental.car.CarEntity;

import javax.smartcardio.CardException;

public class CarPostRequest {

    private String brand;
    private String model;
    private CarStatus carStatus;

    public CarPostRequest(String brand, String model, CarStatus carStatus) {
        this.brand = brand;
        this.model = model;
        this.carStatus = carStatus;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public CarStatus getCarStatus() {
        return carStatus;
    }

    public void setCarStatus(CarStatus carStatus) {
        this.carStatus = carStatus;
    }
}
