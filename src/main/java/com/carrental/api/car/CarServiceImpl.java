package com.carrental.api.car;

import com.carrental.car.CarEntity;
import com.carrental.car.CarRepository;
import com.carrental.car.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@Service
public class CarServiceImpl implements CarService {

    @Autowired
    private CarRepository carRepository;

    @Override
    public CarPostRequest createCar(CarPostRequest carPostRequest) {
        CarEntity carEntity = new CarEntity();
        carEntity.setBrand(carPostRequest.getBrand());
        carEntity.setModel(carPostRequest.getModel());
        carEntity.setCarStatus(carPostRequest.getCarStatus());
        carRepository.save(carEntity);
        return carPostRequest;
    }

    @Override
    public CarGetRequest findCar(Long id) {
        try {
            return carRepository.findById(id).get().toCarGetRequest();
        } catch (NoSuchElementException ex) {
            return null;
        }
    }

    @Override
    public List<CarGetRequest> findAllCars() {
        return carRepository.findAll().stream()
                .map(CarGetRequest::new).collect(Collectors.toList());
    }

    @Override
    public List<CarGetRequest> findAvailableCars() {
        return carRepository.findByCarStatus(CarStatus.AVAILABLE).stream()
                .map(CarGetRequest::new).collect(Collectors.toList());
    }


    @Override
    public void deleteCar(Long id) {
        try {
            carRepository.deleteById(id);
        } catch (EmptyResultDataAccessException ex) {
            throw new CarNotFoundException(id);
        } catch (Exception ex) {
            throw new CarDeleteException(id);
        }
    }

    @Override
    @Transactional
    public void setCarStatus(Long id, CarSetStatusRequest carSetStatusRequest) {
        carRepository.findById(id).get().setCarStatus(carSetStatusRequest.getCarStatus());
    }

    public boolean exist(Long id) {
        try {
            carRepository.findById(id).get();
        } catch (NoSuchElementException ex) {
            return false;
        }
        return true;
    }
}
