package com.carrental.api.car;

public enum CarStatus {
    AVAILABLE,
    UNAVAILABLE;
}
