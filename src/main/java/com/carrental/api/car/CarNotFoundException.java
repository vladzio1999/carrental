package com.carrental.api.car;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class CarNotFoundException extends NullPointerException {
    public CarNotFoundException(Long id) {
        super("Car " + id + " not found");
    }
}
