package com.carrental.api.car.controller;

import com.carrental.api.car.CarGetRequest;
import com.carrental.api.car.CarNotFoundException;
import com.carrental.api.car.CarPostRequest;
import com.carrental.api.car.CarSetStatusRequest;
import com.carrental.car.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping(CarController.BASE_URL)
public class CarController {

    @Autowired
    private CarService carService;

    public static final String BASE_URL = "/cars";


    @GetMapping
    ResponseEntity<List<CarGetRequest>> getAllCars() {
        return new ResponseEntity<>(carService.findAllCars(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    ResponseEntity<CarGetRequest> getCar(@PathVariable Long id) {
        CarGetRequest car = carService.findCar(id);
        if (car == null) {
            throw new CarNotFoundException(id);
        }
        return new ResponseEntity<>(car, HttpStatus.OK);
    }

    @GetMapping("/available")
    ResponseEntity<List<CarGetRequest>> getAvailableCar() {
        return new ResponseEntity<>(carService.findAvailableCars(), HttpStatus.OK);
    }

    @PostMapping
    ResponseEntity<CarPostRequest> addCar(@RequestBody CarPostRequest carPostRequest) {
        carService.createCar(carPostRequest);
        return new ResponseEntity<>(carPostRequest, HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    ResponseEntity<HttpStatus> deleteCar(@PathVariable Long id) {
        carService.deleteCar(id);
        return new ResponseEntity<HttpStatus>(HttpStatus.ACCEPTED);
    }

    @PutMapping("/{id}")
    ResponseEntity<?> setCarStatus(@PathVariable Long id, @RequestBody CarSetStatusRequest carSetStatusRequest) {
        carService.setCarStatus(id, carSetStatusRequest);
        return new ResponseEntity<>(carSetStatusRequest, HttpStatus.OK);
    }

}
