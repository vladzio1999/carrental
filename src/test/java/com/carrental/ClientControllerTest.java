package com.carrental;

import com.carrental.api.car.CarGetRequest;
import com.carrental.api.client.ClientGetRequest;
import com.carrental.api.client.ClientPostRequest;
import com.carrental.api.client.ClientServiceImpl;
import com.carrental.api.client.controller.ClientController;
import com.carrental.api.order.OrderClientGetRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(ClientController.class)
public class ClientControllerTest {

    private MockMvc mockMvc;

    @Autowired
    WebApplicationContext webApplicationContext;

    @MockBean
    ClientServiceImpl clientService;

    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
    }

    @Test
    public void getClientByIdTest() throws Exception {
        when(clientService.findClient(1L))
                .thenReturn(new ClientGetRequest(1L, "Biba", "Boba", "20.12.1985"));
        mockMvc.perform(get("/clients/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value("Biba"))
                .andExpect(jsonPath("$.lastName").value("Boba"))
                .andExpect(jsonPath("$.dateOfBirth").value("20.12.1985"));
        verify(clientService, times(1)).findClient(1L);
        verifyNoMoreInteractions(clientService);
    }

    @Test
    public void testGetClientByIdFail404NotFound() throws Exception {
        mockMvc.perform(get("/clients/{id}", 1L))
                .andDo(print())
                .andExpect(status().isNotFound());
        verify(clientService, times(1)).findClient(1L);
        verifyNoMoreInteractions(clientService);
    }

    @Test
    public void getAllClientsTest() throws Exception{
        List<ClientGetRequest> clients = Arrays.asList(
                new ClientGetRequest(1L, "Biba", "Boba", "20.12.1999"),
                new ClientGetRequest(2L, "Lolik", "Bolik", "12.09.2000"));
        when(clientService.findAllClients()).thenReturn(clients);
        mockMvc.perform(get("/clients"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id").value(1L))
                .andExpect(jsonPath("$[0].firstName").value("Biba"))
                .andExpect(jsonPath("$[0].lastName").value("Boba"))
                .andExpect(jsonPath("$[0].dateOfBirth").value("20.12.1999"))
                .andExpect(jsonPath("$[1].id").value(2L))
                .andExpect(jsonPath("$[1].firstName").value("Lolik"))
                .andExpect(jsonPath("$[1].lastName").value("Bolik"))
                .andExpect(jsonPath("$[1].dateOfBirth").value("12.09.2000"));
        verify(clientService, times(1)).findAllClients();
        verifyNoMoreInteractions(clientService);
    }

    @Test
    public void testGetAllOrders() throws Exception{
        List<OrderClientGetRequest> orders = Arrays.asList(
                new OrderClientGetRequest(1L, new CarGetRequest(), 6),
                new OrderClientGetRequest(2L, new CarGetRequest(), 16)
        );
        when(clientService.findAllOrders(1L)).thenReturn(orders);
        mockMvc.perform(get("/clients/{id}/orders", 1L))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].car").exists())
                .andExpect(jsonPath("$[0].term").value(6))
                .andExpect(jsonPath("$[1].id").value(2))
                .andExpect(jsonPath("$[1].car").exists())
                .andExpect(jsonPath("$[1].term").value(16));
    }

    @Test
    public void testCreateClient() throws Exception {
        mockMvc.perform(post("/clients")
                .content(asJsonString(new ClientPostRequest("Biba", "Boba", "20.12.1999")))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.firstName").exists());
    }

    @Test
    public void testEditClient() throws Exception {
        mockMvc.perform(put("/clients/{id}", 1L)
                .content(asJsonString(new ClientPostRequest("Lolik", "Bolik", "20.12.1987")))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value("Lolik"))
                .andExpect(jsonPath("$.lastName").value("Bolik"))
                .andExpect(jsonPath("$.dateOfBirth").value("20.12.1987"));
    }

    @Test
    public void testDeleteClient() throws Exception {
        mockMvc.perform(delete("/clients/{id}", 1L))
                .andExpect(status().isAccepted());
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
