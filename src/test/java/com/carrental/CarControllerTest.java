package com.carrental;

import com.carrental.api.car.*;
import com.carrental.api.car.controller.CarController;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.*;

@RunWith(SpringRunner.class)
@WebMvcTest(CarController.class)
public class CarControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @MockBean
    private CarServiceImpl carService;

    @Before
    public void setUp() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
    }

    @Test
    public void testGetCarById() throws Exception {

        when(carService.findCar(1L)).thenReturn(new CarGetRequest(1L,"VAZ", "2101", CarStatus.AVAILABLE));
        mockMvc.perform(get("/cars/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.brand").value("VAZ"))
                .andExpect(jsonPath("$.model").value("2101"))
                .andExpect(jsonPath("$.carStatus").value("AVAILABLE"));
        verify(carService, times(1)).findCar(1L);
        verifyNoMoreInteractions(carService);
    }

    @Test
    public void testGetCarByIdFail404NotFound() throws Exception {
        mockMvc.perform(get("/cars/{id}", 1L))
                .andDo(print())
                .andExpect(status().isNotFound());
        verify(carService, times(1)).findCar(1L);
        verifyNoMoreInteractions(carService);
    }

    @Test
    public void testGetAllCars() throws Exception {
        List<CarGetRequest> cars = Arrays.asList(
                new CarGetRequest(1L, "Audi", "100", CarStatus.AVAILABLE),
                new CarGetRequest(2L, "VAZ", "2101", CarStatus.UNAVAILABLE));
        when(carService.findAllCars()).thenReturn(cars);
        mockMvc.perform(get("/cars"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id").value(1L))
                .andExpect(jsonPath("$[0].brand").value("Audi"))
                .andExpect(jsonPath("$[0].model").value("100"))
                .andExpect(jsonPath("$[0].carStatus").value("AVAILABLE"))
                .andExpect(jsonPath("$[1].id").value(2L))
                .andExpect(jsonPath("$[1].brand").value("VAZ"))
                .andExpect(jsonPath("$[1].model").value("2101"))
                .andExpect(jsonPath("$[1].carStatus").value("UNAVAILABLE"));
        verify(carService, times(1)).findAllCars();
        verifyNoMoreInteractions(carService);
    }


    @Test
    public void testGetAvailableCars() throws Exception {
        List<CarGetRequest> cars = Arrays.asList(
                new CarGetRequest(1L, "Audi", "100", CarStatus.UNAVAILABLE),
        new CarGetRequest(2L, "VAZ", "2101", CarStatus.AVAILABLE));
        when(carService.findAvailableCars()).thenReturn(cars.subList(1,2));
        mockMvc.perform(get("/cars/available"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").value(2L))
                .andExpect(jsonPath("$[0].brand").value("VAZ"))
                .andExpect(jsonPath("$[0].model").value("2101"))
                .andExpect(jsonPath("$[0].carStatus").value("AVAILABLE"));
        verify(carService, times(1)).findAvailableCars();
        verifyNoMoreInteractions(carService);
    }

    @Test
    public void testCreateCar() throws Exception {
        mockMvc.perform(post("/cars")
                .content(asJsonString(new CarPostRequest("BMW", "M5", CarStatus.AVAILABLE)))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.brand").exists());
    }

    @Test
    public void testEditCarStatus() throws Exception {
        mockMvc.perform(put("/cars/{id}", 1L)
                .content(asJsonString(new CarSetStatusRequest(CarStatus.UNAVAILABLE)))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.carStatus").value("UNAVAILABLE"));
    }

    @Test
    public void testDeleteCar() throws Exception {
        mockMvc.perform(delete("/cars/{id}", 1L))
                .andExpect(status().isAccepted());
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
