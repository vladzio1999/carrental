package com.carrental;

import com.carrental.api.car.CarGetRequest;
import com.carrental.api.car.CarStatus;
import com.carrental.api.client.ClientGetRequest;
import com.carrental.api.order.OrderGetRequest;
import com.carrental.api.order.OrderPostRequest;
import com.carrental.api.order.OrderServiceImpl;
import com.carrental.api.order.controller.OrderController;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(OrderController.class)
public class OrderControllerTest {

    private MockMvc mockMvc;

    @MockBean
    private OrderServiceImpl orderService;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void getOrderByIdTest() throws Exception {
        ClientGetRequest client = new ClientGetRequest(1L, "Biba", "Boba", "20.12.1985");
        CarGetRequest car = new CarGetRequest(1L,"VAZ", "2101", CarStatus.UNAVAILABLE);
        when(orderService.findOrder(1L)).thenReturn(new OrderGetRequest(1L, client, car, 7));
        mockMvc.perform(get("/orders/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.clientGetRequest").isNotEmpty())
                .andExpect(jsonPath("$.carGetRequest").isNotEmpty())
                .andExpect(jsonPath("$.term").value(7));
        verify(orderService, times(2)).findOrder(1L);
        verifyNoMoreInteractions(orderService);
    }

    @Test
    public void testGetOrderByIdFail404NotFound() throws Exception {
        mockMvc.perform(get("/orders/{id}", 5L))
                .andDo(print())
                .andExpect(status().isNotFound());
        verify(orderService, times(1)).findOrder(5L);
        verifyNoMoreInteractions(orderService);
    }

    @Test
    public void testGetAllOrders() throws Exception {
        ClientGetRequest client1 = new ClientGetRequest(1L, "Biba", "Boba", "20.12.1985");
        CarGetRequest car1 = new CarGetRequest(1L,"VAZ", "2101", CarStatus.UNAVAILABLE);
        ClientGetRequest client2 = new ClientGetRequest(2L, "Lolik", "Bolik", "20.12.1985");
        CarGetRequest car2 = new CarGetRequest(2L,"VAVAZ", "2101", CarStatus.UNAVAILABLE);
        List<OrderGetRequest> orders = Arrays.asList(
                new OrderGetRequest(1L, client1, car1, 8),
                new OrderGetRequest(2L, client2, car2, 10));
        when(orderService.findAllOrders()).thenReturn(orders);
        mockMvc.perform(get("/orders"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].clientGetRequest").isNotEmpty())
                .andExpect(jsonPath("$[0].carGetRequest").isNotEmpty())
                .andExpect(jsonPath("$[0].term").value(8))
                .andExpect(jsonPath("$[1].clientGetRequest").isNotEmpty())
                .andExpect(jsonPath("$[1].carGetRequest").isNotEmpty())
                .andExpect(jsonPath("$[1].term").value(10));
        verify(orderService, times(1)).findAllOrders();
        verifyNoMoreInteractions(orderService);
    }

    @Test
    public void testCreateOrder() throws Exception {
        mockMvc.perform(post("/orders")
                .content(asJsonString(new OrderPostRequest(1L, 1L, 1L, 8)))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").exists());
    }

    @Test
    public void testEditOrder() throws Exception {
        mockMvc.perform(put("/orders/{id}", 1L)
                .content(asJsonString(new OrderPostRequest(1L, 2L, 2L, 10)))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.clientId").value(2));
    }

    @Test
    public void testDeleteOrder() throws Exception {
        mockMvc.perform(delete("/orders/{id}", 1L))
                .andExpect(status().isAccepted());
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
