package com.carrental.api;

import com.carrental.api.car.CarServiceImpl;
import com.carrental.api.car.CarStatus;
import com.carrental.api.client.ClientServiceImpl;
import com.carrental.api.order.OrderGetRequest;
import com.carrental.api.order.OrderPostRequest;
import com.carrental.api.order.OrderServiceImpl;
import com.carrental.car.CarEntity;
import com.carrental.car.CarRepository;
import com.carrental.client.ClientEntity;
import com.carrental.client.ClientRepository;
import com.carrental.order.OrderEntity;
import com.carrental.order.OrderRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OrderControllerTestUsingDB {

    private MockMvc mockMvc;

    @Autowired
    WebApplicationContext webApplicationContext;

    @Autowired
    OrderRepository orderRepository;

    @Autowired
    CarRepository carRepository;

    @Autowired
    ClientRepository clientRepository;

    @Autowired
    OrderServiceImpl orderService;

    @Autowired
    CarServiceImpl carService;

    @Autowired
    ClientServiceImpl clientService;

    @Before
    public void setUp() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
    }

    @Test
    public void testGetOrderByIdFromDB() throws Exception {
        CarEntity car = new CarEntity("Audi", "100", CarStatus.AVAILABLE);
        carRepository.save(car);
        ClientEntity client = new ClientEntity("Biba", "Boba", "20.12.1199");
        clientRepository.save(client);
        OrderEntity orderEntity = new OrderEntity(car, client, 7 );
        orderRepository.save(orderEntity);

        String orderObject = asJsonString(orderEntity.toOrderGetRequest());

        mockMvc.perform(get("/orders/{id}", orderEntity.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(orderObject));
    }

    @Test
    public void testGetOrderByIdFail404NotFound() throws Exception {
        mockMvc.perform(get("/orders/{id}", 19L))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGetAllOrdersFromDB() throws Exception {
        ClientEntity clientEntity = new ClientEntity("Petro", "Petrov", "20.02.1998");
        clientRepository.save(clientEntity);
        CarEntity carEntity = new CarEntity("Audi", "100", CarStatus.UNAVAILABLE);
        carRepository.save(carEntity);

        OrderEntity orderEntity1 = new OrderEntity(carEntity, clientEntity, 8);
        orderRepository.save(orderEntity1);
        OrderEntity orderEntity2 = new OrderEntity(carEntity, clientEntity, 19);
        orderRepository.save(orderEntity2);
        List<OrderGetRequest> orders = orderService.findAllOrders();

        String ordersString = asJsonString(orders);

        mockMvc.perform(get("/orders"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(content().json(ordersString));
    }

    @Test
    public void testCreateOrderInDB() throws Exception {
        ClientEntity clientEntity = new ClientEntity("Petro", "Petrov", "20.02.1998");
        clientRepository.save(clientEntity);
        CarEntity carEntity = new CarEntity("Audi", "100", CarStatus.AVAILABLE);
        carRepository.save(carEntity);
        mockMvc.perform(post("/orders")
                .content(asJsonString(new OrderPostRequest(1L, 1L, 8)))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.clientId").value(1));
    }

    @Test
    public void testEditOrderInDB() throws Exception {
        ClientEntity clientEntity = new ClientEntity("Ivan", "Ivanov", "20.12.1998");
        clientRepository.save(clientEntity);
        CarEntity carEntity = new CarEntity("VAZ", "2101", CarStatus.AVAILABLE);
        carRepository.save(carEntity);
        CarEntity carEntity1 = new CarEntity("VAzaaa", "super", CarStatus.AVAILABLE);
        carRepository.save(carEntity1);
        OrderEntity orderEntity = new OrderEntity(carEntity, clientEntity, 8);
        orderRepository.save(orderEntity);

        mockMvc.perform(put("/orders/{id}", orderEntity.getId())
                .content(asJsonString(new OrderPostRequest(clientEntity.getId(), carEntity1.getId(), 19)))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.carId").value(carEntity1.getId()))
                .andExpect(jsonPath("$.clientId").value(clientEntity.getId()))
                .andExpect(jsonPath("$.term").value(19));
    }

    @Test
    public void testDeleteOrderFromDB() throws Exception {
        if (!orderService.exist(1L)) {
            ClientEntity clientEntity = new ClientEntity("Petro", "Petrov", "20.02.1998");
            clientRepository.save(clientEntity);
            CarEntity carEntity = new CarEntity("Audi", "100", CarStatus.UNAVAILABLE);
            carRepository.save(carEntity);
            orderRepository.save(new OrderEntity(carEntity, clientEntity, 8));
        }

        mockMvc.perform(delete("/orders/{id}", 1L))
                .andExpect(status().isAccepted());
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
