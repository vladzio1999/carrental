package com.carrental.api;

import com.carrental.api.car.*;
import com.carrental.car.CarEntity;
import com.carrental.car.CarRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CarControllerTestUsingDB {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    CarRepository carRepository;

    @Autowired
    CarServiceImpl carService;

    @Before
    public void setUp() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
    }

    @Test
    public void testGetCarByIdFromDB() throws Exception {
        CarEntity carEntity = new CarEntity("Audi", "100", CarStatus.AVAILABLE);
        carRepository.save(carEntity);

        String carObject = asJsonString(carService.findCar(carEntity.getId()));

        mockMvc.perform(get("/cars/{id}", carEntity.getId())
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(carObject));
    }

    @Test
    public void testGetCarByIdFail404NotFound() throws Exception {
        mockMvc.perform(get("/cars/{id}", 19L))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGetAllCarsFromDB() throws Exception {
        CarEntity carEntity1 = new CarEntity("Renault", "Duster", CarStatus.AVAILABLE);
        carRepository.save(carEntity1);
        CarEntity carEntity2 = new CarEntity("VAZ", "2101", CarStatus.UNAVAILABLE);
        carRepository.save(carEntity2);

        List<CarGetRequest> cars = carService.findAllCars();

        String carsString = asJsonString(cars);

        mockMvc.perform(get("/cars"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(content().json(carsString));
    }

    @Test
    public void testGetAvailableCarsFromDB() throws Exception {
        CarEntity carEntity = new CarEntity("Opel", "Frontera", CarStatus.AVAILABLE);
        carRepository.save(carEntity);

        List<CarGetRequest> cars = carService.findAvailableCars();

        String carsString = asJsonString(cars);

        mockMvc.perform(get("/cars/available"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(content().json(carsString));
    }

    @Test
    public void testCreateCarInDB() throws Exception {
        mockMvc.perform(post("/cars")
                .content(asJsonString(new CarPostRequest("BMW", "M5", CarStatus.AVAILABLE)))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.brand").exists());
    }

    @Test
    public void testEditCarStatus() throws Exception {
        if (!carService.exist(1L)) {
            carRepository.save(new CarEntity("BMW", "M5", CarStatus.AVAILABLE));
        }

        mockMvc.perform(put("/cars/{id}", 1L)
                .content(asJsonString(new CarSetStatusRequest(CarStatus.UNAVAILABLE)))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.carStatus").value("UNAVAILABLE"));
    }

    @Test
    public void testDeleteCarFromDB() throws Exception {
        if (!carService.exist(1L)) {
            carRepository.save(new CarEntity("BMW", "M5", CarStatus.AVAILABLE));
        }

        mockMvc.perform(delete("/cars/{id}", 1L))
                .andExpect(status().isAccepted());
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
