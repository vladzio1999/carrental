package com.carrental.api;

import com.carrental.api.client.ClientGetRequest;
import com.carrental.api.client.ClientPostRequest;
import com.carrental.api.client.ClientServiceImpl;
import com.carrental.client.ClientEntity;
import com.carrental.client.ClientRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@RunWith(SpringRunner.class)
@SpringBootTest
public class ClientControllerTestUsingDB {

    private MockMvc mockMvc;

    @Autowired
    WebApplicationContext webApplicationContext;

    @Autowired
    ClientRepository clientRepository;

    @Autowired
    ClientServiceImpl clientService;

    @Before
    public void setUp() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
    }

    @Test
    public void testGetClientByIdFromDB() throws Exception {
        ClientEntity clientEntity = new ClientEntity("Biba", "Boba", "20.12.1999");
        clientRepository.save(clientEntity);

        String clientObject = asJsonString(clientService.findClient(clientEntity.getId()));

        mockMvc.perform(get("/clients/{id}", clientEntity.getId())
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(clientObject));
    }

    @Test
    public void testGetClientByIdFail404NotFound() throws Exception {
        mockMvc.perform(get("/clients/{id}", 19L))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGetAllClientsFromDB() throws Exception {
        ClientEntity clientEntity1 = new ClientEntity("Petro", "Petrov", "20.02.1998");
        clientRepository.save(clientEntity1);
        ClientEntity clientEntity2 = new ClientEntity("Ivan", "Ivanov", "12.01.1988");
        clientRepository.save(clientEntity2);

        List<ClientGetRequest> clients = clientService.findAllClients();

        String clientsString = asJsonString(clients);

        mockMvc.perform(get("/clients"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(content().json(clientsString));
    }

    @Test
    public void testCreateClientInDB() throws Exception {
        mockMvc.perform(post("/clients")
                .content(asJsonString(new ClientPostRequest("Ivan", "Ivanov", "20.01.1999")))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.firstName").exists());
    }

    @Test
    public void testEditClientInDB() throws Exception {
        ClientEntity clientEntity = new ClientEntity("Ivan", "Ivanov", "20.12.1998");
        clientRepository.save(clientEntity);

        mockMvc.perform(put("/clients/{id}", clientEntity.getId())
                .content(asJsonString(new ClientPostRequest("Ivanko",
                        "Ivanchenko", "20.12.1998")))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value("Ivanko"))
                .andExpect(jsonPath("$.lastName").value("Ivanchenko"))
                .andExpect(jsonPath("$.dateOfBirth").value("20.12.1998"));
    }

    @Test
    public void testDeleteClientFromDB() throws Exception {
        if (!clientService.exist(1L)) {
            clientRepository.save(new ClientEntity("Petro", "Petrenko", "12.01.1945"));
        }

        mockMvc.perform(delete("/clients/{id}", 1L))
                .andExpect(status().isAccepted());
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
